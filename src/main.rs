mod reszip;
use crate::reszip::*;
use clap::Parser;

/*Steps:
1. Walk directory
2. get archive name based on directory name
3. compress each file using flate2
3. encrpyt each file
4. pack files of the sam folder into a archive named to that folder e.g. all sound files get zip into sounds.tar
*/

//Clap
#[derive(Parser, Debug)]
#[command(author= "Peter Keller", version="0.1.0", about= "ResZip - Resource Packer", long_about = None)]
struct Args {
    #[arg(long)]
    compress: bool,
    #[arg(long)]
    encrypt: bool,
    #[arg(short, long)]
    out_dir: String,
    #[arg(long)]
    save_config: bool,
    #[arg(long)]
    save_packerassets: bool,
}

impl From<Args> for PackerConfig {
    fn from(value: Args) -> Self {
        Self {
            compress: value.compress,
            encrypt: value.encrypt,
            out_dir: value.out_dir,
        }
    }
}

fn main() {
    let args = Args::parse();
    let save_config = args.save_config;
    let save_packerassets = args.save_packerassets;

    let mut reszip = ResZipApp::new(
        "./resources/".into(),
        args.into(),
        save_config,
        save_packerassets,
    );

    reszip.run();
}
