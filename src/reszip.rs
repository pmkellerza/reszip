//Core
use std::collections::HashMap;
use std::fs;
use std::io::prelude::*;
use std::io::BufWriter;
use std::path::Path;
use std::sync::Arc;

//filesystem ulitiy
use walkdir::WalkDir;

//Compression
use flate2::write::ZlibEncoder;
use flate2::Compression;

//encryption
use magic_crypt::{new_magic_crypt, MagicCryptTrait};

//archiving
extern crate tar;
use tar::{Builder, Header};

//Serialization
use ron::ser::PrettyConfig;
use serde::{Deserialize, Serialize};

///Type Aliases
pub type ArchiveName = String;
//pub type AssetPath = Arc<str>;
pub type AssetPath = String;
pub type PackerAssets = HashMap<String, PackerAsset>;

#[derive(Debug, Deserialize, Serialize)]
pub struct PackerConfig {
    pub compress: bool,
    pub encrypt: bool,
    pub out_dir: String,
}

impl PackerConfig {
    pub fn default() -> PackerConfig {
        PackerConfig {
            compress: true,
            encrypt: false,
            out_dir: "./out".to_string(),
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PackerAsset {
    path: AssetPath,
    archive_name: ArchiveName,
    filenames: Vec<String>,
}

#[derive(Debug)]
pub struct ResZipApp {
    config: PackerConfig,
    assets_dir: Arc<str>,
    packer_assets: PackerAssets,
    save_config: bool,
    save_packerassets: bool,
}

impl ResZipApp {
    pub fn new(
        assets_dir: Arc<str>,
        config: PackerConfig,
        save_config: bool,
        save_packerassets: bool,
    ) -> Self {
        Self {
            config,
            assets_dir,
            packer_assets: PackerAssets::new(),
            save_config,
            save_packerassets,
        }
    }

    pub fn run(&mut self) {
        //We populate packer_assets that stores path to each folder and archive name
        self.populate_packerassets();
        //println!("{:#?}", self.packer_assets);

        //here using path from packer_assets than compress, encyrpt, save files to tar archive
        self.pack_assets();

        //print Packer_assets
        println!("{:#?}", self.packer_assets);

        //Save PackerAssets to ron file
        self.save_data();
    }

    fn populate_packerassets(&mut self) {
        let dir = self.assets_dir.clone();
        let walkdir = WalkDir::new(&*dir);

        for entry in walkdir {
            let entry = entry.unwrap();
            if entry.path().is_dir() {
                let path = entry.path().to_str().unwrap();

                let (id, archive_name) = self.get_archive_name(&path);
                if !id.is_empty() {
                    let packer_asset = PackerAsset {
                        path: path.into(),
                        archive_name,
                        filenames: Vec::new(),
                    };
                    self.packer_assets.insert(id, packer_asset);
                }
            }
        }
    }
    fn get_archive_name(&mut self, path: &str) -> (String, String) {
        let name = path.split("/").last().unwrap_or_default();
        (name.to_string(), format!("{}.pak", name)) // Append ".pak" for TAR archive
    }

    fn pack_assets(&mut self) {
        for (_id, packer_asset) in &mut self.packer_assets {
            //create each archive file for packing
            let out_path = self.config.out_dir.as_str(); //folder where tar archives will be in
            let archive_path = format!("{}/{}", out_path, packer_asset.archive_name);

            //check if out_path directory exist create if not
            if !Path::new(out_path).exists() {
                std::fs::DirBuilder::new().create(out_path).unwrap();
            }

            let archive_file = fs::OpenOptions::new()
                .create(true)
                .write(true)
                .open(archive_path.clone())
                .unwrap();

            //tar builder
            let mut archive = Builder::new(archive_file);
            let mut header = Header::new_gnu();

            //we walk the path for files
            let walkdir = WalkDir::new(&*packer_asset.path);

            for entry in walkdir {
                let entry = entry.unwrap();

                if entry.path().is_file() {
                    //println!("Entry: {:?}", entry.file_name());

                    //only files are processed
                    let mut file = fs::File::open(entry.path()).unwrap();

                    let mut file_data = Vec::new();
                    file.read_to_end(&mut file_data).unwrap();

                    drop(file); //close file

                    //println!("Data Len: {}", file_data.len());

                    //compress data
                    if self.config.compress {
                        file_data = compress_data(file_data);
                    }

                    //encrypt data
                    if self.config.encrypt {
                        file_data = encrypt_data(file_data);
                    }

                    //pack into tar archive
                    header.set_size(file_data.len() as u64);
                    header.set_cksum();

                    packer_asset
                        .filenames
                        .push(entry.file_name().to_str().unwrap().to_string());

                    let split: Vec<&str> = entry.file_name().to_str().unwrap().split(".").collect();
                    let filename = split[0];

                    archive
                        .append_data(&mut header, filename, file_data.as_slice())
                        .unwrap()
                }
            }
        }
    }

    fn save_data(&self) {
        let path = format!("{}/packassets.ron", self.config.out_dir);
        let cfg_path = format!("{}/config.ron", self.config.out_dir);

        if self.save_packerassets {
            //asset pack
            let asset_file = fs::OpenOptions::new()
                .create(true)
                .write(true)
                .open(path)
                .unwrap();

            let _res = ron::ser::to_writer_pretty(
                BufWriter::new(asset_file),
                &self.packer_assets,
                PrettyConfig::default(),
            );
        }

        if self.save_config {
            //config
            let cfg_file = fs::OpenOptions::new()
                .create(true)
                .write(true)
                .open(cfg_path)
                .unwrap();

            //serialize data to ron file
            let _res = ron::ser::to_writer_pretty(
                BufWriter::new(cfg_file),
                &self.config,
                PrettyConfig::default(),
            );
        }
    }
}

fn compress_data(data: Vec<u8>) -> Vec<u8> {
    let mut encoder = ZlibEncoder::new(Vec::new(), Compression::default());
    let buf = data.as_slice();
    encoder.write_all(buf).expect("TODO: panic message");
    let compressed_data = encoder.finish().unwrap();

    compressed_data
}

fn encrypt_data(data: Vec<u8>) -> Vec<u8> {
    let mc = new_magic_crypt!("VEXARGAMEENGINE", 64);
    let encrypted_data = mc.encrypt_bytes_to_base64(data.as_slice());
    encrypted_data.as_bytes().to_vec()
}
